package com.lendenclub.utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import io.github.bonigarcia.wdm.WebDriverManager;

public class WebCapability {
	
	static WebDriver driver;

	public WebDriver WebCapability() {
		
		ChromeOptions opt = new ChromeOptions();
		WebDriverManager.chromedriver().setup();
		opt.addArguments("headless");
		driver = new ChromeDriver(opt);
		return driver;
		
	}

}
