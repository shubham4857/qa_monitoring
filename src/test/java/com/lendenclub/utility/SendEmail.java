package com.lendenclub.utility;

import java.util.Properties;
import javax.activation.DataHandler;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

public class SendEmail {

	@SuppressWarnings("unused")
	public SendEmail(String recipient, String subject, String[] columnName, String[] statusCount) throws Exception {
		
		Message message = null;
	    final String sender = "sandeep.singh@lendenclub.com";
	    final String password="swamiji@786s";
	  
	    Properties properties = System.getProperties();
	    properties.put("mail.smtp.auth", "true");
	    properties.put("mail.smtp.starttls.enable", "true");
	    properties.put("mail.smtp.host", "smtp.gmail.com");
	    properties.put("mail.smtp.port", "587");
	    
	    Authenticator auth =new Authenticator(){	
	    protected PasswordAuthentication getPasswordAuthentication(){
	    	
	    	return new PasswordAuthentication(sender, password);	
	         }
	    };
	    Session session = Session.getInstance(properties, auth);
	    
	    
	    try {
		    message = new MimeMessage(session);
	        message.setFrom(new InternetAddress(sender)); 
	        message.setRecipients(Message.RecipientType.TO ,  InternetAddress.parse(recipient)); 
	        message.setSubject(subject);
	    
	        Multipart multipart = new MimeMultipart();
	       
	        String filename=null;
	        if(filename!=null)
	        {
	        	  BodyPart attachmentBodyPart = new MimeBodyPart();   
	        	  attachmentBodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(filename.getBytes(),"text/csv")));
	              attachmentBodyPart.setFileName("Failed Customer");
	              multipart.addBodyPart(attachmentBodyPart);
	              System.out.println("File attached");	
	        }
	        
	        BodyPart messageBodyPart = new MimeBodyPart(); 
	        
	        int size=statusCount.length;
	        makingMessageBodyPart(messageBodyPart,size,columnName,statusCount);
	        
	        multipart.addBodyPart(messageBodyPart);       
	        message.setContent(multipart);
	     }
			catch (MessagingException mex) {
				mex.printStackTrace();
			}
			Transport.send(message);
			System.out.println("Mail successfully sent");

		}

	private void makingMessageBodyPart(BodyPart messageBodyPart, int size, String[] columnName, String[] statusCount) throws Exception {
		
		if(size == 1) {
			
			 messageBodyPart.setContent(""
		        		//+ "<h2> Payment Link Status Count:</h2>"
		        		+ "<table style=\"border-spacing: 15px;\">"
		        		  + "<thead>"
		        	    	+ "<tr>"
		        		       + " <th >"+columnName[0]+" </th> "
		        		    + "</tr>"
		        		  + "</thead>"
		        		  + " <tdata>"
		        		    + "<tr> "
		        		      + "<td>"+ statusCount[0] +"</td>"
		        		    + "</tr>"
		        	      + "</tdata>"
		        		+ "</table>"
		        		,"text/html");
			
		} else if (size == 2){
			
			 messageBodyPart.setContent(""
		        		//+ "<h2> Payment Link Status Count:</h2>"
		        		+ "<table style=\"border-spacing: 15px;\">"
		        		  + "<thead>"
		        	    	+ "<tr>"
		        		       + " <th >"+columnName[0]+" </th> <th> "+columnName[1]+" </th> "
		        		    + "</tr>"
		        		  + "</thead>"
		        		  + " <tdata>"
		        		    + "<tr> "
		        		      + "<td>"+ statusCount[0] +"</td>  <td>"+ statusCount[1]+"</td> "
		        		    + "</tr>"
		        	      + "</tdata>"
		        		+ "</table>"
		        		,"text/html");
			
		} else if (size == 3) {
			
			 messageBodyPart.setContent(""
		        		//+ "<h2> Payment Link Status Count:</h2>"
		        		+ "<table style=\"border-spacing: 15px;\">"
		        		  + "<thead>"
		        	    	+ "<tr>"
		        		       + " <th >"+columnName[0]+" </th> <th> "+columnName[1]+" </th> <th>  " +columnName[2]+"  </th> "
		        		    + "</tr>"
		        		  + "</thead>"
		        		  + " <tdata>"
		        		    + "<tr> "
		        		      + "<td>"+ statusCount[0] +"</td>  <td>"+ statusCount[1]+"</td> <td> "+statusCount[2]+"</td> "
		        		    + "</tr>"
		        	      + "</tdata>"
		        		+ "</table>"
		        		,"text/html");
			
		} else if (size == 4)
		{
			messageBodyPart.setContent(""
	        		//+ "<h2> Payment Link Status Count:</h2>"
	        		+ "<table style=\"border-spacing: 15px;\">"
	        		  + "<thead>"
	        	    	+ "<tr>"
	        		       + " <th >"+columnName[0]+" </th> <th> "+columnName[1]+" </th> <th>  " +columnName[2]+"  </th>  <th>  " +columnName[3]+"  </th>"
	        		    + "</tr>"
	        		  + "</thead>"
	        		  + " <tdata>"
	        		    + "<tr> "
	        		      + "<td>"+ statusCount[0] +"</td>  <td>"+ statusCount[1]+"</td> <td> "+statusCount[2]+"</td>  <td> "+statusCount[3]+"</td> "
	        		    + "</tr>"
	        	      + "</tdata>"
	        		+ "</table>"
	        		,"text/html");
			
		} else {
			
			messageBodyPart.setContent("<h1>There is no data<h1>","text/html");
			
		}
		
		
	}
	
	

		
	

}
