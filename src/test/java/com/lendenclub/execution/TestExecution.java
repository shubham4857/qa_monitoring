package com.lendenclub.execution;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.lendenclub.steps.BorrowersInSalesExecutive;
import com.lendenclub.steps.Login;
import com.lendenclub.steps.PaymentLink;
import com.lendenclub.steps.VerifyInterestRate_CT;
import com.lendenclub.utility.WebCapability;

public class TestExecution extends WebCapability{
	
	WebDriver driver;
	@BeforeTest
	public void openingBrowser() throws Exception
	{
		this.driver = WebCapability();
		new Login(driver);
	}
	
//	@Test(priority = 1)
//	public void PaymentLinkPage() throws Exception
//	{
//		new PaymentLink(driver);
//	}
	
	@Test(priority = 1)
	public void CT_InterestRatePage() throws Exception
	{
		new VerifyInterestRate_CT(driver);
	}
	
	@Test(priority = 2)
	public void BorrowersSalesExecutivePage() throws Exception
	{
		new BorrowersInSalesExecutive(driver);
	}
	
	@AfterTest
	public void CloseBrowser()
	{
		driver.quit();
	}
	
}
