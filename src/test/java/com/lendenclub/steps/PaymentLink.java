package com.lendenclub.steps;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.lendenclub.utility.SendEmail;

public class PaymentLink {
	
	WebDriver driver;
	String URL="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MzU5NSwiZmlsdGVyIjpbImFuZCIsWyI9IixbImZpZWxkLWlkIiw0MzI4N10sIkNPTVBMRVRFRCJdLFsidGltZS1pbnRlcnZhbCIsWyJmaWVsZC1pZCIsNDMyNzRdLCJjdXJyZW50IiwiZGF5Il1dLCJhZ2dyZWdhdGlvbiI6W1siY291bnQiXV19LCJkYXRhYmFzZSI6Mn0sImRpc3BsYXkiOiJzY2FsYXIiLCJ2aXN1YWxpemF0aW9uX3NldHRpbmdzIjp7fX0=";
	String status[]= {"PROCESSING","EXPIRED","PENDING","COMPLETED"};
	String receipent ="qateam@lendenclub.com";
	public PaymentLink(WebDriver driver) throws Exception {
		
		this.driver=driver;
		driver.navigate().to(URL);
		int statusLength=status.length;
		String statusCount[]=new String[statusLength];
		
		for(int i=0;i<statusLength; i++)
		{
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a")).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div/div/div/ul[1]/li[1]/a")).click();
			driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div/div/div/ul[1]/li/input")).sendKeys(status[i]);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.xpath("//*[contains(text(),'Update filter')]")).click();
			driver.manage().timeouts().implicitlyWait(23, TimeUnit.SECONDS);
			statusCount[i]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();		
		}
		
		new SendEmail(receipent,"Payment Link Status Count",status,statusCount);
		
	}

}