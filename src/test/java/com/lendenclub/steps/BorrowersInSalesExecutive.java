package com.lendenclub.steps;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import com.lendenclub.utility.SendEmail;

public class BorrowersInSalesExecutive {
	
	WebDriver driver;
	String URL="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MTcyLCJqb2lucyI6W3siZmllbGRzIjoiYWxsIiwic291cmNlLXRhYmxlIjoxMTEsImNvbmRpdGlvbiI6WyI9IixbImZpZWxkLWlkIiw2MDZdLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIFJlcXVpcmVkbG9hbiIsWyJmaWVsZC1pZCIsMTI5MF1dXSwiYWxpYXMiOiJMZW5kZW5hcHAgUmVxdWlyZWRsb2FuIn1dLCJmaWx0ZXIiOlsiYW5kIixbIj0iLFsiZmllbGQtaWQiLDYwMl0sMjE2MzFdLFsiPSIsWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgUmVxdWlyZWRsb2FuIixbImZpZWxkLWlkIiwxMzE4XV0sIk9QRU4iXSxbIj0iLFsiZmllbGQtaWQiLDU5OV0sIjExIl0sWyI9IixbImZpZWxkLWlkIiw2MTFdLDExXSxbIj0iLFsiZmllbGQtaWQiLDYwN10sMTFdXSwiYWdncmVnYXRpb24iOltbImNvdW50Il1dfSwiZGF0YWJhc2UiOjJ9LCJkaXNwbGF5Ijoic2NhbGFyIiwidmlzdWFsaXphdGlvbl9zZXR0aW5ncyI6e319";
	String columnData[]= {"Count"};
	String receipent ="qateam@lendenclub.com";
	
	public BorrowersInSalesExecutive(WebDriver driver) throws Exception {
		
		this.driver=driver;
		((JavascriptExecutor)driver).executeScript("window.open()");
	    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
	    driver.switchTo().window(tabs.get(2));
		driver.navigate().to(URL);
		
		String countData[]=new String[1];
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		countData[0]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		int count=Integer.parseInt(countData[0]);
		if(count > 0)
		{
			new SendEmail(receipent, "Borrowers in sales Executive after completed 11 Steps  ", columnData, countData);
		} else {
			
			System.out.println(" Every thing is fine, No need to send email For Sales Executive project");
		}
		
	}

}
