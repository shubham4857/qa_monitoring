package com.lendenclub.steps;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import com.lendenclub.utility.SendEmail;

public class VerifyInterestRate_CT {
	
	WebDriver driver;
	String URL="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MTA2MSwiZmlsdGVyIjpbImFuZCIsWyJjb250YWlucyIsWyJmaWVsZC1pZCIsMTE3MTFdLCJFY29YLUJMLTEwMSIseyJjYXNlLXNlbnNpdGl2ZSI6ZmFsc2V9XSxbIjwiLFsiZmllbGQtaWQiLDExNzE2XSwzNF1dLCJhZ2dyZWdhdGlvbiI6W1siY291bnQiXV19LCJkYXRhYmFzZSI6OX0sImRpc3BsYXkiOiJzY2FsYXIiLCJ2aXN1YWxpemF0aW9uX3NldHRpbmdzIjp7fX0=";
    String columnData[]= {"Count"};
	String receipent ="qateam@lendenclub.com";

	public VerifyInterestRate_CT(WebDriver driver) throws Exception {
		
		this.driver=driver;
		((JavascriptExecutor)driver).executeScript("window.open()");
	    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
	    driver.switchTo().window(tabs.get(1));
		driver.navigate().to(URL);
		driver.navigate().refresh();
		String countData[]=new String[1];
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		countData[0]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		int count=Integer.parseInt(countData[0]);
		if(count > 0)
		{
			new SendEmail(receipent, "Interest Rate less than 34 % For CT ", columnData, countData);
		} else {
			
			System.out.println(" Every thing is fine, No need to send email for CT");
		}
		
	}

}
